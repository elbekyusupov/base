from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from apps.accounts.models.user import User


# Register your models here.
@admin.register(User)
class UserAdmin(UserAdmin):
    list_display = ('id', 'username', 'email', 'is_superuser', 'date_joined')
    list_display_links = ('id', 'username', 'email', 'is_superuser', 'date_joined')
    search_fields = ('username', 'email')
